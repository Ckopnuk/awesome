#include "sprite_reader.h"

#include <algorithm>
#include <iomanip>
#include <iostream>

Sprite_reader::Sprite_reader() {}

void Sprite_reader::load_sprites(std::vector<Sprite>& sprites, std::istream& in,
                                 Ckom::Engine& texture_cahe)
{
    Sprite spr;

    std::string atrib_name;

    while (!in.eof() && in >> atrib_name)
    {
        if ("uv_rect:" == atrib_name)
        {
            Rect r;
            in >> r.pos.x >> r.pos.y >> r.size.x >> r.size.y;
            spr.uv_rect(r);
        }
        else if ("world_pos:" == atrib_name)
        {
            Ckom::vec2 pos;
            in >> pos.x >> pos.y;
            spr.pos(pos);
        }
        else if ("size:" == atrib_name)
        {
            Ckom::vec2 size;
            in >> size.x >> size.y;
            spr.size(size);
        }
        else if ("angle:" == atrib_name)
        {
            float ang;
            in >> ang;
            spr.rotation(ang);

            sprites.push_back(spr);
        }
        else if ("id:" == atrib_name)
        {
            std::string name;
            in >> name;
            spr.id(name);
        }
        else if ("texture:" == atrib_name)
        {
            std::string name;
            in >> name;
            Ckom::Texture* texture = texture_cahe.create_texture(name);
            spr.texture(texture);
        }

        in >> std::ws;
    }
    return;
}

void Sprite_reader::save_sprites(const std::vector<Sprite>& list,
                                 std::ostream&              out)
{
    const auto save_on_sprite = [&out](const Sprite& spr)
    {
        out << std::left << std::setw(12) << "id: " << spr.id() << '\n';

        const Ckom::Texture* texture = spr.texture();
        if (texture == nullptr)
        {
            throw std::runtime_error{ "Error: no texture in sprite" };
        }
        const std::string_view name = texture->get_name();
        out << std::left << std::setw(12) << "texture: " << name << '\n';
        out << std::left << std::setw(12) << "uv_rect: ";
        const Rect& r = spr.uv_rect();
        // clang-format off
        out << std::left << std::setprecision(3) << std::fixed
            << std::setw(7) << r.pos.x << ' '
            << std::setw(7) << r.pos.x << ' '
            << std::setw(7) << r.size.x << ' '
            << std::setw(7) << r.size.y << '\n';
        // clang-format on
        out << std::left << std::setw(12) << "world_pos: " << spr.pos().x << ' '
            << spr.pos().y << '\n';
        out << std::left << std::setw(12) << "size: " << spr.size().x << ' '
            << spr.size().y << '\n';
        out << std::left << std::setw(12) << "angle: " << spr.rotation()
            << '\n';
    };

    std::for_each(std::begin(list), std::end(list), save_on_sprite);
}
