#ifndef TANK_H
#define TANK_H

#include "Timer.h"
#include "bullet.h"
#include "game_obj.h"

#include <math.h>
#include <vector>

class Gun;

class Tank : public Game_obj
{
public:
    Tank(Ckom::Engine* eng);
    virtual ~Tank();

    virtual void         render() override;
    virtual Ckom::mat2x3 matrix() override;

    float get_direction() const;
    void  set_direction(float value);

    //    void set_move(float speed);

    Gun* gun;

    Timer timer;

    float get_radius() const;

    //    int main_hp;

protected:
    float direction = 0;
    float radius    = 0.05;
    //    Ckom::Sound_buffer* sound_truck;
};

class Gun : public Game_obj
{
public:
    Gun(Ckom::Engine* eng);
    ~Gun();

    void         render() override;
    Ckom::mat2x3 matrix() override;

    void set_pos_on_tank(const Ckom::vec2 tower_pos);
    void fire();

    std::vector<Bullet*> bullets;

    float get_dir() const;
    void  set_dir(float value);

    float get_gun_timer_volue() const;
    void  set_gun_timer_volue(float value);

protected:
    float gun_timer_volue = 0.0f;
    float dir             = 0.0f;
    float bullet_speed    = 0.02;
};

#endif // TANK_H
