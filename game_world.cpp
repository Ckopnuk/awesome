#include "game_world.h"

#include <sstream>

Game_World::Game_World(Ckom::Engine* eng)
{
    engine = eng;
    if (!engine)
    {
        std::cerr << "in world engine is null" << std::endl;
    }
}

Game_World::~Game_World()
{
    if (!enemies.empty())
    {
        enemies.at(0)->destroy();
        for (std::size_t i = 0; i < enemies.size(); ++i)
        {
            delete enemies.at(i);
        }
    }

    enemies.clear();
    cannon->destroy();
    delete cannon;
    b_ground->destroy();
    delete b_ground;
}

string Game_World::init_game(std::string_view)
{
    std::stringstream err_world;

    b_ground = new Ground(engine);
    if (!b_ground)
    {
        err_world << "ERROR b_ground is nullptr" << std::endl;
        return err_world.str();
    }

    cannon = new Tank(engine);
    if (!cannon)
    {
        err_world << "Cannon is nullptr" << std::endl;
        return err_world.str();
    }

    cannon->set_pos(0, -0.9);

    int i = 0;

    for (i = 0; i < enemies_count; ++i)
    {
        if (i < first_wave)
        {
            Enemy* enemy = new Enemy_litle(*engine);
            enemy->set_pos(-0.9f + (i * 0.1), 1);
            enemies.push_back(enemy);
        }
        else if (i >= first_wave && i < enemies_count)
        {
            float  n     = (i - (i % 10)) * 0.01;
            Enemy* enemy = new Enemy_litle(*engine);
            enemy->set_pos(-0.9 + ((i % 10) * 0.1), 1 + n);
            enemies.push_back(enemy);
        }
    }

    for (i = enemies_count; i < enemies_avarge_count; ++i)
    {
        Enemy_avarge* enemy_avarge = new Enemy_avarge(*engine);
        enemy_avarge->set_pos(1, 0.7 - ((i % 10) * 0.3));
        enemies.push_back(enemy_avarge);
    }

    std::ifstream file_text;
    file_text.exceptions(std::ios::badbit | std::ios::failbit);
    file_text.open("src/anim/text/text.yaml", std::ios::binary);
    Sprite_reader loader_of_sprites;
    loader_of_sprites.load_sprites(sprites, file_text, *engine);
    //  file_text.close();

    enemies_left = 43;
    return "";
}

void Game_World::render_world()
{

    b_ground->render();
    cannon->render();
    for (std::size_t i = 0; i < enemies.size(); ++i)
    {
        enemies.at(i)->render();
    }
}

float Game_World::get_game_speed() const
{
    return game_speed;
}

void Game_World::set_game_speed(float value)
{
    game_speed = value;
}

int Game_World::get_enemies_count() const
{
    return enemies_count;
}

void Game_World::set_enemies_count(int value)
{
    enemies_count = value;
}

Tank* Game_World::get_cannon() const
{
    return cannon;
}

void Game_World::apply_damage()
{
    if (!cannon->gun->bullets.empty() && !enemies.empty())
    {
        for (std::size_t i = 0; i < cannon->gun->bullets.size(); ++i)
        {

            for (std::size_t j = 0; j < enemies.size(); ++j)
            {
                double radius =
                    cannon->gun->bullets[i]->radius + enemies[j]->get_radius();
                double xa = cannon->gun->bullets[i]->get_pos().x;
                double ya = cannon->gun->bullets[i]->get_pos().y;
                double xb = enemies[j]->get_pos().x;
                double yb = enemies[j]->get_pos().y;

                double distance = sqrt(pow((xb - xa), 2) + pow((yb - ya), 2));
                if (radius >= distance)
                {
                    enemies.at(j)->hp -= 1;
                    delete cannon->gun->bullets[i];
                    cannon->gun->bullets.erase(cannon->gun->bullets.begin() +
                                               i);
                    if (enemies.at(j)->hp == 0)
                    {
                        delete enemies[j];
                        enemies.erase(enemies.begin() + j);
                        enemies_left -= 1;
                    }
                    break;
                }
            }
        }
    }
}

bool Game_World::encounter()
{
    bool result = false;
    if (!enemies.empty())
    {
        for (std::size_t i = 0; i < enemies.size(); ++i)
        {
            collision_whith_the_border(enemies.at(i));

            for (std::size_t j = 0; j < enemies.size(); ++j)
            {

                if (i != j)
                {
                    double radius =
                        enemies[i]->get_radius() + enemies[j]->get_radius();
                    double xa = enemies[i]->get_pos().x;
                    double ya = enemies[i]->get_pos().y;
                    double xb = enemies[j]->get_pos().x;
                    double yb = enemies[j]->get_pos().y;

                    double distance =
                        sqrt(pow((xb - xa), 2) + pow((yb - ya), 2));
                    if (radius >= distance)
                    {
                        enemies[i]->set_change_default_move(true);
                        enemies[j]->set_change_default_move(true);
                        result   = true;
                        float xj = enemies.at(i)->get_pos().x -
                                   enemies.at(j)->get_pos().x;
                        float yj = enemies.at(i)->get_pos().y -
                                   enemies.at(j)->get_pos().y;

                        float xi = enemies.at(j)->get_pos().x -
                                   enemies.at(i)->get_pos().x;
                        float yi = enemies.at(j)->get_pos().y -
                                   enemies.at(i)->get_pos().y;

                        float modul_j = sqrt(xj * xj + yj * yj);
                        float modul_i = sqrt(xi * xi + yi * yi);

                        float cosinus_aj = xj / modul_j;
                        float cosinus_ai = xi / modul_i;

                        float angle_j = acos(cosinus_aj);
                        if (yj < 0)
                        {
                            angle_j = -acos(cosinus_aj);
                        }

                        float angle_i = acos(cosinus_ai);
                        if (yi < 0)
                        {
                            angle_i = -acos(cosinus_ai);
                        }

                        enemies.at(j)->set_direction(angle_j);
                        enemies.at(i)->set_direction(angle_i);

                        float mx = enemies.at(j)->get_speed() *
                                   cos(enemies.at(j)->get_direction());
                        float my = enemies.at(j)->get_speed() *
                                   sin(enemies.at(j)->get_direction());
                        enemies.at(j)->set_move_dir(Ckom::vec2{ mx, my });

                        float mx_i = enemies.at(i)->get_speed() *
                                     cos(enemies.at(i)->get_direction());
                        float my_i = enemies.at(i)->get_speed() *
                                     sin(enemies.at(i)->get_direction());
                        enemies.at(j)->set_move_dir(Ckom::vec2{ mx_i, my_i });
                    }
                }
            }
        }
    }
    return result;
}

void Game_World::controller()
{
    for (size_t i = 0; i < enemies.size(); ++i)
    {
        enemies.at(i)->set_move(enemies.at(i)->get_move_dir().x,
                                enemies.at(i)->get_move_dir().y);
    }
    encounter();
}

void Game_World::collision_whith_the_border(Enemy* target) const
{
    Ckom::vec2 border_min{ -1, -1 };
    Ckom::vec2 border_max{ 1, 1 };
    if (target->get_pos().x <= border_min.x)
    {
        float xj = -target->get_pos().x - border_min.x;
        float yj = -target->get_pos().y - target->get_pos().y;

        float modul_j = sqrt(xj * xj + yj * yj);

        float cosinus_aj = xj / modul_j;

        float angle_j = acos(cosinus_aj);
        if (yj < 0)
        {
            angle_j = -acos(cosinus_aj);
        }

        target->set_direction(angle_j);

        float mx = target->get_speed() * cos(target->get_direction());
        float my = target->get_speed() * sin(target->get_direction());
        target->set_move_dir(Ckom::vec2{ mx, my });
    }

    if (target->get_pos().y <= border_min.y)
    {
        float xj = -target->get_pos().x - target->get_pos().x;
        float yj = -target->get_pos().y - border_min.y;

        float modul_j = sqrt(xj * xj + yj * yj);

        float cosinus_aj = xj / modul_j;

        float angle_j = acos(cosinus_aj);
        if (yj < 0)
        {
            angle_j = -acos(cosinus_aj);
        }

        target->set_direction(angle_j);

        float mx = target->get_speed() * cos(target->get_direction());
        float my = target->get_speed() * sin(target->get_direction());
        target->set_move_dir(Ckom::vec2{ mx, my });
    }

    if (target->get_pos().x >= border_max.x)
    {
        float xj = -target->get_pos().x - border_max.x;
        float yj = -target->get_pos().y - target->get_pos().y;

        float modul_j = sqrt(xj * xj + yj * yj);

        float cosinus_aj = xj / modul_j;

        float angle_j = acos(cosinus_aj);
        if (yj < 0)
        {
            angle_j = -acos(cosinus_aj);
        }

        target->set_direction(angle_j);

        float mx = target->get_speed() * cos(target->get_direction());
        float my = target->get_speed() * sin(target->get_direction());
        target->set_move_dir(Ckom::vec2{ mx, my });
    }

    if (target->get_pos().y >= border_max.y)
    {
        float xj = -target->get_pos().x - target->get_pos().x;
        float yj = -target->get_pos().y - border_max.y;

        float modul_j = sqrt(xj * xj + yj * yj);

        float cosinus_aj = xj / modul_j;

        float angle_j = acos(cosinus_aj);
        if (yj < 0)
        {
            angle_j = -acos(cosinus_aj);
        }

        target->set_direction(angle_j);

        float mx = target->get_speed() * cos(target->get_direction());
        float my = target->get_speed() * sin(target->get_direction());
        target->set_move_dir(Ckom::vec2{ mx, my });
    }
}

void Game_World::draw_sec(const int sec)
{
    switch (sec % 10)
    {
        case 0:
            sprites.at(0).set_world_pos({ 0.9, 0.9 });
            sprites.at(0).size({ 0.1, 0.1 });
            sprites.at(0).draw(*engine);
            break;
        case 1:
            sprites.at(1).set_world_pos({ 0.9, 0.9 });
            sprites.at(1).size({ 0.1, 0.1 });
            sprites.at(1).draw(*engine);
            break;
        case 2:
            sprites.at(2).set_world_pos({ 0.9, 0.9 });
            sprites.at(2).size({ 0.1, 0.1 });
            sprites.at(2).draw(*engine);
            break;
        case 3:
            sprites.at(3).set_world_pos({ 0.9, 0.9 });
            sprites.at(3).size({ 0.1, 0.1 });
            sprites.at(3).draw(*engine);
            break;
        case 4:
            sprites.at(4).set_world_pos({ 0.9, 0.9 });
            sprites.at(4).size({ 0.1, 0.1 });
            sprites.at(4).draw(*engine);
            break;
        case 5:
            sprites.at(5).set_world_pos({ 0.9, 0.9 });
            sprites.at(5).size({ 0.1, 0.1 });
            sprites.at(5).draw(*engine);
            break;
        case 6:
            sprites.at(6).set_world_pos({ 0.9, 0.9 });
            sprites.at(6).size({ 0.1, 0.1 });
            sprites.at(6).draw(*engine);
            break;
        case 7:
            sprites.at(7).set_world_pos({ 0.9, 0.9 });
            sprites.at(7).size({ 0.1, 0.1 });
            sprites.at(7).draw(*engine);
            break;
        case 8:
            sprites.at(8).set_world_pos({ 0.9, 0.9 });
            sprites.at(8).size({ 0.1, 0.1 });
            sprites.at(8).draw(*engine);
            break;
        case 9:
            sprites.at(9).set_world_pos({ 0.9, 0.9 });
            sprites.at(9).size({ 0.1, 0.1 });
            sprites.at(9).draw(*engine);
            break;
    }

    switch ((sec % 100) / 10)
    {
        case 0:
            sprites.at(0).set_world_pos({ 0.8, 0.9 });
            sprites.at(0).size({ 0.1, 0.1 });
            sprites.at(0).draw(*engine);
            break;
        case 1:
            sprites.at(1).set_world_pos({ 0.8, 0.9 });
            sprites.at(1).size({ 0.1, 0.1 });
            sprites.at(1).draw(*engine);
            break;
        case 2:
            sprites.at(2).set_world_pos({ 0.8, 0.9 });
            sprites.at(2).size({ 0.1, 0.1 });
            sprites.at(2).draw(*engine);
            break;
        case 3:
            sprites.at(3).set_world_pos({ 0.8, 0.9 });
            sprites.at(3).size({ 0.1, 0.1 });
            sprites.at(3).draw(*engine);
            break;
        case 4:
            sprites.at(4).set_world_pos({ 0.8, 0.9 });
            sprites.at(4).size({ 0.1, 0.1 });
            sprites.at(4).draw(*engine);
            break;
        case 5:
            sprites.at(5).set_world_pos({ 0.8, 0.9 });
            sprites.at(5).size({ 0.1, 0.1 });
            sprites.at(5).draw(*engine);
            break;
        case 6:
            sprites.at(6).set_world_pos({ 0.8, 0.9 });
            sprites.at(6).size({ 0.1, 0.1 });
            sprites.at(6).draw(*engine);
            break;
        case 7:
            sprites.at(7).set_world_pos({ 0.8, 0.9 });
            sprites.at(7).size({ 0.1, 0.1 });
            sprites.at(7).draw(*engine);
            break;
        case 8:
            sprites.at(8).set_world_pos({ 0.8, 0.9 });
            sprites.at(8).size({ 0.1, 0.1 });
            sprites.at(8).draw(*engine);
            break;
        case 9:
            sprites.at(9).set_world_pos({ 0.8, 0.9 });
            sprites.at(9).size({ 0.1, 0.1 });
            sprites.at(9).draw(*engine);
            break;
    }

    if (sec == 100)
    {
        sprites.at(1).set_world_pos({ 0.7, 0.9 });
        sprites.at(1).size({ 0.1, 0.1 });
        sprites.at(1).draw(*engine);
    }
}

void Game_World::uninitialize()
{
    delete b_ground;
    delete cannon;
    if (!enemies.empty())
    {
        for (size_t i = 0; i < enemies.size(); ++i)
        {
            delete enemies.at(i);
        }
        enemies.clear();
    }
}

std::vector<Enemy*> Game_World::get_enemies() const
{
    return enemies;
}

int Game_World::get_enemies_left() const
{
    return enemies_left;
}
