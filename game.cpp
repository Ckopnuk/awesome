#pragma GCC diagnostic push
// turn off the specific warning. Can also use "-Wall"
#pragma GCC diagnostic ignored "-Wall"
#include "src/imgui/imgui.h"
#pragma GCC diagnostic pop

#include "game_world.h"

int main(int /*argc*/, char* /*argv*/[])
{
    Ckom::Engine*     engine = Ckom::create_engine();
    const std::string error  = engine->initialize("");
    if (!error.empty())
    {
        std::cerr << error << std::endl;
        return EXIT_FAILURE;
    }

    Game_World* world;
    world = new Game_World(engine);

    bool        continue_loop         = true;
    bool        start                 = false;
    bool        lost                  = false;
    bool        won                   = false;
    float       gun_dir               = 0;
    const float pi                    = std::numbers::pi_v<float>;
    float       cannon_rotation_speed = 0.009;
    float       cannon_fire_speed     = 0.2;

    Timer   gun_timer;
    Timer   game_timer;
    Ground* ram = new Ground(engine);

    float timer_value = 0;
    float max_time    = 90;

    while (continue_loop)
    {

        Ckom::Event event;

        while (engine->read_event(event))
        {
            std::cout << event << std::endl;
            switch (event)
            {
                case Ckom::Event::turn_off:
                    continue_loop = false;
                    break;
                default:
                    break;
            }
        }

        if (start == false)
        {
            if (engine->is_key_down(Ckom::Keys::start))
            {
                start = true;
                lost  = false;
                won   = false;

                const std::string world_error = world->init_game("");
                if (!world_error.empty())
                {
                    std::cerr << world_error << std::endl;
                    delete ram;
                    delete world;
                    engine->uninitialize();
                    Ckom::destroy_engine(engine);
                    return EXIT_FAILURE;
                }

                game_timer.reset();
            }

            if (engine->is_key_down(Ckom::Keys::select))
            {
                continue_loop = false;
            }

            if (!lost && !won)
            {
                ram->set_texture("src/texture/ram.png", *engine);
                ram->render();
            }
            if (lost)
            {
                ram->set_texture("src/texture/lose_ram.png", *engine);
                ram->render();
            }
            if (won)
            {
                ram->set_texture("src/texture/won_ram.png", *engine);
                ram->render();
            }

            engine->swap_buffers();
        }
        else
        {

            if (engine->is_key_down(Ckom::Keys::select))
            {
                continue_loop = false;
            }

            if (engine->is_key_down(Ckom::Keys::up_a))
            {
                if (world->get_cannon()->gun->get_gun_timer_volue() >=
                    cannon_fire_speed)
                {
                    gun_timer.reset();
                    world->get_cannon()->gun->fire();
                }
            }

            if (engine->is_key_down(Ckom::Keys::right_a))
            {
                gun_dir -= pi * cannon_rotation_speed;
                world->get_cannon()->gun->set_dir(gun_dir);
            }
            if (engine->is_key_down(Ckom::Keys::left_a))
            {
                gun_dir += pi * cannon_rotation_speed;
                world->get_cannon()->gun->set_dir(gun_dir);
            }

            timer_value = max_time - game_timer.elapsed();

            world->apply_damage();
            world->get_cannon()->gun->set_gun_timer_volue(gun_timer.elapsed());
            world->controller();
            world->render_world();
            world->draw_sec(timer_value);

            engine->swap_buffers();

            if (engine->is_key_down(Ckom::Keys::bCross) || timer_value <= 0)
            {
                start = false;
                lost  = true;
                world->uninitialize();
            }

            if (world->get_enemies_left() == 0)
            {
                start = false;
                won   = true;
                world->uninitialize();
            }
        }
    }

    delete ram;
    delete world;

    engine->uninitialize();
    Ckom::destroy_engine(engine);

    return EXIT_SUCCESS;
}
