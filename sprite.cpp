#include "sprite.h"
#include <array>
#include <numbers>

Sprite::Sprite()
    : id_("__no__id__error:__")
{
}

Sprite::Sprite(const std::string_view id, Ckom::Texture* tex,
               const Rect& rect_on_texture, const Ckom::vec2& pos,
               const Ckom::vec2& size, const float angle)
    : id_(id)
    , texture_(tex)
    , uv_rect_(rect_on_texture)
    , pos_(pos)
    , size_(size)
    , rotation_(angle)
{
}

void Sprite::draw(Ckom::Engine& render)
{
    if (texture_ == nullptr)
    {
        return; // sprite is empty nothing to do
    }

    ///   0            1
    ///   *------------*
    ///   |           /|
    ///   |         /  |
    ///   |      P/    |  // P - pos_ or center of sprite
    ///   |     /      |
    ///   |   /        |
    ///   | /          |
    ///   *------------*
    ///   3            2
    ///

    using namespace Ckom;
    Vertex_pct vertex[4];
    // open gl tex lower left angle is (0,0) coord
    vertex[0].tx = uv_rect_.pos + vec2(0.f, uv_rect_.size.y);
    vertex[1].tx = uv_rect_.pos + vec2(uv_rect_.size.x, uv_rect_.size.y);
    vertex[2].tx = uv_rect_.pos + vec2(uv_rect_.size.x, 0.f);
    vertex[3].tx = uv_rect_.pos;

    float half_widht  = size_.x / 2;
    float half_height = size_.y / 2;

    vertex[0].p = pos_ + vec2(-half_widht, half_height);
    vertex[1].p = pos_ + vec2(half_widht, half_height);
    vertex[2].p = pos_ + vec2(half_widht, -half_height);
    vertex[3].p = pos_ + vec2(-half_widht, -half_height);

    Color white{ 1, 1, 1, 1 };

    vertex[0].c = white;
    vertex[1].c = white;
    vertex[2].c = white;
    vertex[3].c = white;

    // build triangles to render sprite
    std::array<Ckom::Triangle_pct, 2> triangles;
    Triangle_pct                      tri0;
    triangles.at(0).v[0] = vertex[0];
    triangles.at(0).v[1] = vertex[1];
    triangles.at(0).v[2] = vertex[3];

    Triangle_pct tri1;
    triangles.at(1).v[0] = vertex[1];
    triangles.at(1).v[1] = vertex[2];
    triangles.at(1).v[2] = vertex[3];

    Ckom::Vertex_buffer* vert_buf =
        render.create_vertex_buffer(&triangles[0], triangles.size());

    vec2   screen_size   = render.screen_size();
    float  aspect        = screen_size.y / screen_size.x;
    mat2x3 window_aspect = mat2x3::scale(aspect, 1.0);

    const float pi         = std::numbers::pi_v<float>;
    mat2x3      move_w     = mat2x3::moove(world_pos);
    mat2x3      rotation_w = mat2x3::rotation(rotation_ /** (pi / 180)*/);

    mat2x3 world_transform = window_aspect * rotation_w * move_w;

    render.render(*vert_buf, texture_, world_transform);

    //    render.render(tri0, texture_, world_transform);
    //    render.render(tri1, texture_, world_transform);
}

Ckom::Texture* Sprite::texture() const
{
    return texture_;
}

void Sprite::texture(Ckom::Texture* t)
{
    texture_ = t;
}

const Rect& Sprite::uv_rect() const
{
    return uv_rect_;
}

void Sprite::uv_rect(const Rect& r)
{
    uv_rect_ = r;
}

Ckom::vec2 Sprite::pos() const
{
    return pos_;
}

void Sprite::pos(const Ckom::vec2& p)
{
    pos_ = p;
}

Ckom::vec2 Sprite::size() const
{
    return size_;
}

void Sprite::size(const Ckom::vec2& s)
{
    size_ = s;
}

float Sprite::rotation() const
{
    return rotation_;
}

void Sprite::rotation(const float r)
{
    rotation_ = r;
}

const std::string& Sprite::id() const
{
    return id_;
}

void Sprite::id(std::string_view name)
{
    id_ = name;
}

Ckom::vec2 Sprite::get_world_pos() const
{
    return world_pos;
}

void Sprite::set_world_pos(const Ckom::vec2& value)
{
    world_pos = value;
}
