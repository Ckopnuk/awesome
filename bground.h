#ifndef GROUND_H
#define GROUND_H

#include "game_obj.h"

class Ground : public Game_obj
{
public:
    Ground(Ckom::Engine* eng);
    virtual ~Ground();

    virtual void         render() override;
    virtual Ckom::mat2x3 matrix() override;

    float get_radius() const;

protected:
    float radius = 0.03f;
};

#endif // GROUND_H
