#include "game_obj.h"

Game_obj::Game_obj() {}

Game_obj::~Game_obj() {}

Ckom::Vertex_buffer* Game_obj::get_vertex_buf() const
{
    return vertex_buf;
}

Ckom::Texture* Game_obj::get_texture() const
{
    return texture;
}

Ckom::vec2 Game_obj::get_pos() const
{
    return pos;
}

void Game_obj::set_texture(std::string_view path, Ckom::Engine& eng)
{
    texture = eng.create_texture(path.data());
    if (nullptr == texture)
    {
        std::cerr << "failed load texture: " << path.data();
    }
}

void Game_obj::set_pos(const float x, const float y)
{
    pos.x = x;
    pos.y = y;
}

void Game_obj::destroy()
{
    engine->destroy_texture(texture);
    engine->destroy_vertex_buffer(vertex_buf);
}

void Game_obj::set_vertex_buf(std::string_view path, Ckom::Engine& eng)
{
    std::ifstream file(path.data());
    if (!file)
    {
        std::cerr << "Can't load " << path.data() << std::endl;
    }
    else
    {
        std::array<Ckom::Triangle_pct, 2> triangles;
        file >> triangles[0] >> triangles[1];
        vertex_buf = eng.create_vertex_buffer(&triangles[0], triangles.size());
        if (vertex_buf == 0)
        {
            std::cerr << "can't create vertex bufer" << std::endl;
        }
    }
    file.close();
}
