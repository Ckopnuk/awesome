#ifndef BULLET_H
#define BULLET_H

#include "game_obj.h"

#include <stdexcept>

class Bullet : public Game_obj
{
public:
    Bullet(std::string_view path_to_texture, std::string_view path_to_vb,
           Ckom::Engine* eng, Ckom::vec2& position);
    virtual ~Bullet();

    void         render() override;
    Ckom::mat2x3 matrix() override;

    float get_direction() const;
    void  set_direction(float value);
    void  set_move(float x, float y);
    float get_scale_n() const;
    void  set_scale_n(float value);

    float speed     = 1.5f;
    float direction = 0.f;
    float radius    = 0.04;
    float scale_n   = 0.4;

    Ckom::Sound_buffer* sound_bullet_hit;
};

#endif // BULLET_H
