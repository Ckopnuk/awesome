#include "bground.h"

Ground::Ground(Ckom::Engine* eng)
{
    engine = eng;
    if (engine == nullptr)
    {
        std::cerr << "can't create ground: engine == nullptr!" << std::endl;
    }

    set_texture("src/texture/background.png", *engine);
    set_vertex_buf("src/vertex/vert_tex_color_bgrnd.txt", *engine);
}

Ground::~Ground()
{
    std::cout << "destroy ground" << std::endl;
}

void Ground::render()
{
    engine->render(*vertex_buf, texture, matrix());
}

Ckom::mat2x3 Ground::matrix()
{
    const float  pi       = std::numbers::pi_v<float>;
    Ckom::mat2x3 scale    = Ckom::mat2x3::scale(1, 1 * (1024 / 760));
    Ckom::mat2x3 rotation = Ckom::mat2x3::rotation(2 * pi);
    Ckom::mat2x3 position = Ckom::mat2x3::moove(pos);
    Ckom::mat2x3 res      = scale * rotation * position;
    return res;
}

float Ground::get_radius() const
{
    return radius;
}
