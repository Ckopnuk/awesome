#include "tank.h"

Tank::Tank(Ckom::Engine* eng)
{
    engine = eng;
    set_texture("src/texture/Stand.png", *engine);
    set_vertex_buf("src/vertex/vert_tex_col_tank.txt", *engine);

    gun = new Gun(engine);

    //    main_hp = 100;
}

Tank::~Tank()
{
    std::cout << "destroy tank" << std::endl;
    delete gun;
}

void Tank::render()
{
    engine->render(*vertex_buf, texture, matrix());
    gun->set_pos_on_tank(this->pos);
    gun->render();
}

Ckom::mat2x3 Tank::matrix()
{
    Ckom::mat2x3 scaleEvil = Ckom::mat2x3::scale(0.3);
    Ckom::mat2x3 rotation  = Ckom::mat2x3::rotation(direction);
    Ckom::mat2x3 position  = Ckom::mat2x3::moove(pos);
    Ckom::mat2x3 res       = scaleEvil * rotation * position;
    return res;
}

float Tank::get_direction() const
{
    return direction;
}

void Tank::set_direction(float value)
{
    direction = value;
}

// void Tank::set_move(float speed)
//{
//    pos.x += speed * -sin(direction);
//    pos.y += speed * cos(direction);
//    //    if (timer.elapsed() >= 1)
//    //    {
//    //        sound_truck->play(Ckom::Sound_buffer::properties::looped);
//    //        timer.reset();
//    //    }
//}

float Tank::get_radius() const
{
    return radius;
}

///////////////////////////////// GUN //////////////////////////////////

Gun::Gun(Ckom::Engine* eng)
{
    engine = eng;
    set_texture("src/texture/Cannon.png", *engine);
    set_vertex_buf("src/vertex/tower_vert.txt", *engine);
}

Gun::~Gun()
{
    if (bullets.size() != 0)
    {
        for (size_t i = 0; i < bullets.size(); ++i)
        {
            delete bullets.at(i);
        }
        bullets.clear();
    }
    std::cout << "destroy gun" << std::endl;
}

void Gun::render()
{

    for (size_t i = 0; i < bullets.size(); ++i)
    {
        bullets.at(i)->render();
        float x = bullet_speed * -sin(bullets.at(i)->get_direction());
        float y = bullet_speed * cos(bullets.at(i)->get_direction());
        bullets.at(i)->set_move(x, y);
        if (bullets.at(i)->get_pos().y > 1 || bullets.at(i)->get_pos().x > 1 ||
            bullets.at(i)->get_pos().x < -1)
        {
            delete bullets.at(i);
            bullets.erase(bullets.begin() + i);
        }
    }
    engine->render(*this->vertex_buf, this->texture, matrix());
}

Ckom::mat2x3 Gun::matrix()
{

    Ckom::mat2x3 scale    = Ckom::mat2x3::scale(0.25);
    Ckom::mat2x3 rotation = Ckom::mat2x3::rotation(dir);
    Ckom::mat2x3 position = Ckom::mat2x3::moove(pos);
    Ckom::mat2x3 res      = scale * rotation * (position);
    return res;
}

void Gun::set_pos_on_tank(const Ckom::vec2 tower_pos)
{
    //    this->dir = dir;

    this->pos = tower_pos + Ckom::vec2{ 0, 0.05 };
    this->pos.x += 0.06 * -sin(this->dir);
    this->pos.y += 0.06 * cos(this->dir);
    //    this->pos.x += 0.08 * -sin(this->dir);
    //    this->pos.y += 0.08 * cos(this->dir);
}

void Gun::fire()
{
    Ckom::vec2 position = this->pos;

    Bullet* bullet =
        new Bullet("src/texture/Cannonball.png",
                   "src/vertex/vert_tex_color_bullet.txt", engine, position);
    bullet->set_direction(this->dir);
    bullet->set_scale_n(0.09);
    bullets.push_back(bullet);

    for (size_t i = 0; i < bullets.size(); ++i)
    {
    }
}

float Gun::get_dir() const
{
    return dir;
}

void Gun::set_dir(float value)
{
    dir = value;
}

float Gun::get_gun_timer_volue() const
{
    return gun_timer_volue;
}

void Gun::set_gun_timer_volue(float value)
{
    gun_timer_volue = value;
}
