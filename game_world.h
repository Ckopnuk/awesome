#ifndef GAME_WORLD_H
#define GAME_WORLD_H

#include "bground.h"
#include "enemy.h"
#include "engine.h"
#include "tank.h"

class Game_World
{
public:
    Game_World(Ckom::Engine* eng);
    ~Game_World();

    std::string init_game(std::string_view /*conf*/);
    void        render_world();

    float get_game_speed() const;
    void  set_game_speed(float value);

    int  get_enemies_count() const;
    void set_enemies_count(int value);

    int get_enemies_left() const;

    Tank* get_cannon() const;

    void apply_damage();
    bool encounter();
    void controller();
    void collision_whith_the_border(Enemy* target) const;
    void draw_sec(const int sec);
    void uninitialize();

    std::vector<Enemy*> get_enemies() const;

protected:
    Ckom::Engine*       engine               = nullptr;
    float               game_speed           = 1;
    Ground*             b_ground             = nullptr;
    Tank*               cannon               = nullptr;
    int                 enemies_count        = 40;
    int                 first_wave           = 10;
    int                 enemies_left         = 43;
    int                 enemies_avarge_count = 43;
    std::vector<Sprite> sprites;

    std::vector<Enemy*>        enemies;
    std::vector<Enemy_avarge*> enemies_avarge;

private:
    bool change_move = false;
};

#endif // GAME_WORLD_H
