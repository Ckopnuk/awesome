#include "enemy.h"
#include <chrono>
#include <math.h>
#include <numbers>

const float pi = std::numbers::pi_v<float>;

Enemy_litle::Enemy_litle(Ckom::Engine& eng)
    : Enemy("src/texture/Target.png", "src/vertex/vert_tex_color_enemy_l.txt",
            eng)
{
    speed  = 0.003;
    radius = 0.040;
    dmg    = 1;
    hp     = 1;
}

Enemy_litle::~Enemy_litle()
{
    std::cout << "litle enemy destroy" << std::endl;
}

void Enemy_litle::set_move(float x, float y)
{
    if (change_default_move == false)
    {
        this->pos.y -= speed;
        this->move_to_the_side(0.8, 0.7, false);
        this->move_to_the_side(0.65, 0.55, true);
        this->move_to_the_side(0.5, 0.4);
        this->move_to_the_side(0.35, 0.25, true);
        this->move_to_the_side(0.20, 0.10);
        this->move_to_the_side(0.05, -0.05, true);
        this->move_to_the_side(-0.10, -0.20);
        this->move_to_the_side(-0.25, 0.35, true);
    }
    else
    {
        this->pos.x += x;
        this->pos.y += y;
    }
}

Ckom::mat2x3 Enemy_litle::matrix()
{
    Ckom::mat2x3 scaleEvil = Ckom::mat2x3::scale(0.1, 0.11 * (1024 / 760));
    Ckom::mat2x3 rotation  = Ckom::mat2x3::rotation(pi);
    Ckom::mat2x3 position  = Ckom::mat2x3::moove(pos);
    Ckom::mat2x3 res       = scaleEvil * rotation * position;
    return res;
}

///////////////////////// ENEMY ////////////////////////

Enemy::Enemy(std::string_view path_to_texture, std::string_view path_to_vertex,
             Ckom::Engine& eng)
{
    set_texture(path_to_texture, eng);
    set_vertex_buf(path_to_vertex, eng);
    engine = &eng;
}

Enemy::~Enemy() {}

void Enemy::set_move(bool /*reverse*/) {}

void Enemy::set_move(float x, float y)
{
    this->pos.x += x;
    this->pos.y += y;
}

void Enemy::move_to_the_side(const float height_max, const float height_min,
                             bool left_side)
{
    int side = 0;
    if (left_side == true)
    {
        side = -1;
    }
    else
    {
        side = 1;
    }

    if (this->pos.y <= height_max && this->pos.y >= height_min)
    {
        this->pos.x += speed * 2 * side;
    }
}

void Enemy::set_speed(float sp)
{
    speed = sp;
}

float Enemy::get_speed()
{
    return speed;
}

float Enemy::get_radius()
{
    return radius;
}

int Enemy::get_dmg()
{
    return dmg;
}

float Enemy::get_direction() const
{
    return direction;
}

void Enemy::set_direction(float value)
{
    direction = value;
}

void Enemy::render()
{
    engine->render(*this->get_vertex_buf(), this->get_texture(), matrix());
}

int Enemy::get_hp() const
{
    return hp;
}

void Enemy::set_hp(int value)
{
    hp = value;
}

Ckom::vec2 Enemy::get_move_dir() const
{
    return move_dir;
}

void Enemy::set_move_dir(const Ckom::vec2& value)
{
    move_dir = value;
}

void Enemy::set_change_default_move(bool value)
{
    change_default_move = value;
}

///////////////////////// avarge /////////////////////////////////

Enemy_avarge::Enemy_avarge(Ckom::Engine& eng)
    : Enemy("src/texture/Target.png", "src/vertex/vert_tex_col_avarge.txt", eng)
{
    speed     = 0.005;
    radius    = 0.110;
    dmg       = 2;
    hp        = 2;
    direction = pi;
    move_dir  = Ckom::vec2{ -speed, 0 };
}

Enemy_avarge::~Enemy_avarge()
{
    //    std::cerr << "avarge destroy" << std::endl;
}

void Enemy_avarge::set_move(float x, float y)
{
    this->pos.x += x;
    this->pos.y += y;
}

Ckom::mat2x3 Enemy_avarge::matrix()
{
    Ckom::mat2x3 scaleEvil = Ckom::mat2x3::scale(0.25, 0.27 * (1024 / 760));
    Ckom::mat2x3 rotation  = Ckom::mat2x3::rotation(direction);
    Ckom::mat2x3 position  = Ckom::mat2x3::moove(pos);
    Ckom::mat2x3 res       = scaleEvil * rotation * position;
    return res;
}
