#ifndef SPRITE_READER_H
#define SPRITE_READER_H

#include <istream>
#include <vector>

#include "sprite.h"

class Sprite_reader
{
public:
    Sprite_reader();

    void load_sprites(std::vector<Sprite>&, std::istream& in,
                      Ckom::Engine& texture_cahe);
    void save_sprites(const std::vector<Sprite>&, std::ostream& out);
};

#endif // SPRITE_READER_H
