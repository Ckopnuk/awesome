#ifndef ENEMY_H
#define ENEMY_H

#include "game_obj.h"
#include "sprite.h"
#include "sprite_reader.h"

class Enemy : public Game_obj
{
public:
    Enemy(std::string_view path_to_texture, std::string_view path_to_vertex,
          Ckom::Engine& en);
    virtual ~Enemy();

    virtual void set_move(bool reverse = false);
    virtual void set_move(float x, float y);

    virtual void  set_speed(float sp);
    virtual float get_speed();
    virtual float get_radius();
    virtual int   get_dmg();

    virtual void render() override;

    virtual int  get_hp() const;
    virtual void set_hp(int value);

    float get_direction() const;
    void  set_direction(float value);

    int hp;

    Ckom::vec2 get_move_dir() const;
    void       set_move_dir(const Ckom::vec2& value);

    void set_change_default_move(bool value);

protected:
    float      speed  = 0.001;
    float      radius = 0.2;
    int        dmg;
    Ckom::vec2 move_dir;
    float      direction;
    bool       change_default_move = false;

    void move_to_the_side(const float height_max, const float height_min,
                          bool left_side = false);
};

class Enemy_litle final : public Enemy
{
public:
    Enemy_litle(Ckom::Engine& eng);
    ~Enemy_litle() override;

    void set_move(float x, float y) override;

    Ckom::mat2x3 matrix() override final;
};

class Enemy_avarge : public Enemy
{
public:
    Enemy_avarge(Ckom::Engine& eng);
    ~Enemy_avarge();

    void set_move(float x, float y) override;

    Ckom::mat2x3 matrix() override;
};

#endif // ENEMY_H
